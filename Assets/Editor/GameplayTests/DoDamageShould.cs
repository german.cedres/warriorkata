using Gameplay.Domain;
using Gameplay.Domain.Actions;
using NSubstitute;
using NUnit.Framework;
using UniRx;
using static Editor.GameplayTests.Mothers.WarriorMother;
using static Etermax.TestDriveRx.GivenContext;

namespace Editor.GameplayTests

{
    public class DoDamageShould
    {
        [Test]
        public void SendOnDamageDone()
        {
            var onDamageDone = new Subject<int>();
            
            Given(new DoDamage(onDamageDone))
                .When(action => action.Do(new []{AWarrior()}))
                .Then(_ => onDamageDone, it => it.EmittedOnce())
                .Run();
        }
        
        [Test]
        public void CallGetUnitsFromGameRepository()
        {
            var onDamageDone = new Subject<int>();
            var gameRepository = Substitute.For<GameRepository>();
            
            Given(new DoDamage(onDamageDone))
                .When(action => action.Do(new []{AWarrior()}))
                .Then(_ => gameRepository.Received(1).GetWarriors())
                .Run();
        }
    }
}