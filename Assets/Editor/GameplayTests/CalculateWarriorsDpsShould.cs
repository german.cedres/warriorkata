using System.Collections.Generic;
using Gameplay.Domain.Actions;
using Gameplay.Domain.WarriorKata;
using NSubstitute;
using NUnit.Framework;
using UniRx;
using static Editor.GameplayTests.Mothers.GameRepositoryMother;
using static Etermax.TestDriveRx.GivenContext;

namespace Editor.GameplayTests
{
    [TestFixture]
    public class CalculateWarriorsDpsShould
    {
        [Test]
        public void SendEvent()
        {
            var onWarriorsDpsCalculated = new Subject<IEnumerable<Warrior>>();
            
            Given(new CalculateWarriorsDps(onWarriorsDpsCalculated, AGameRepository()))
                .When(action => action.Do())
                .Then(_ => onWarriorsDpsCalculated, it => it.EmittedOnce())
                .Run();
        }
        [Test]
        public void CallGetWarriorsFromRepository()
        {
            var onWarriorsDpsCalculated = new Subject<IEnumerable<Warrior>>();
            var gameRepository = AGameRepository();
            
            Given(new CalculateWarriorsDps(onWarriorsDpsCalculated, AGameRepository()))
                .When(action => action.Do())
                .Then( _ => gameRepository.Received(1).GetWarriors())
                .Run();
        }
    }
}