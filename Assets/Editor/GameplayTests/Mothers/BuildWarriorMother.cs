using Gameplay.Domain.WarriorKata;
using UniRx;
using static Editor.GameplayTests.Mothers.WarriorBuilderServiceMother;
using static Editor.GameplayTests.Mothers.WarriorInProgressRepositoryMother;

namespace Editor.GameplayTests.Mothers
{
    public static class BuildWarriorMother
    {
        public static BuildWarrior ABuildWarrior(
            ISubject<Warrior> withWarriorBuilt = null,
            WarriorBuilderService withWarriorBuilderService = null,
            WarriorInProgressRepository withWarriorInProgressRepository = null
        ) {
            
            return new BuildWarrior(
                withWarriorBuilt ?? new Subject<Warrior>(), 
                withWarriorBuilderService ?? AWarriorBuilderService(),
                withWarriorInProgressRepository ?? AWarriorInProgressRepository());
        }
    }
}