using System;
using Gameplay.Domain.WarriorKata;
using NSubstitute;
using UniRx;

namespace Editor.GameplayTests.Mothers
{
    public static class WarriorInProgressRepositoryMother
    {
        public static WarriorInProgressRepository AWarriorInProgressRepository()
        {
            var repository = Substitute.For<WarriorInProgressRepository>();
            repository.GetClass().Returns(Observable.Return(WarriorClass.NotChosen));
            repository.GetName().Returns(Observable.Return(""));
            repository.GetWeapons().Returns(Observable.Return(new Weapons()));
            return repository;
        }

        public static WarriorInProgressRepository AInMemoryWarriorInProgressRepository(
            WarriorClass? withWarriorClass = null,
            string withName = null,
            Weapons? withWeapons = null
        ) =>
            new InMemoryWarriorInProgressRepository(
                withWarriorClass ?? WarriorClass.NotChosen,
                withName ?? "",
                withWeapons ?? new Weapons()
            );
    }
}