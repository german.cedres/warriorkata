using Gameplay.Domain.WarriorKata;
using NSubstitute;
using UniRx;
using static Editor.GameplayTests.Mothers.WarriorMother;

namespace Editor.GameplayTests.Mothers
{
    public static class WarriorBuilderServiceMother
    {
        public static WarriorBuilderService AWarriorBuilderService()
        {
            var service = Substitute.For<WarriorBuilderService>();
            service.AddName(Arg.Any<Warrior>(), Arg.Any<string>())
                .Returns(Observable.Return(AWarrior()));
            service.AddWeapons(Arg.Any<Warrior>(), Arg.Any<Weapons>())
                .Returns(Observable.Return(AWarrior()));
            service.AddWarriorClass(Arg.Any<Warrior>(), Arg.Any<WarriorClass>())
                .Returns(Observable.Return(AWarrior()));
            service.CreateEmptyWarrior()
                .Returns(Observable.Return(AWarrior()));
            return service;
        }

        public static WarriorBuilderService ALocalWarriorBuilderService()
        {
            return new LocalWarriorBuilderService();
        }


    }
}