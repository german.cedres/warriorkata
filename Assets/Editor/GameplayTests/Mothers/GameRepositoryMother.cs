using System.Linq;
using Gameplay.Domain;
using Gameplay.Domain.WarriorKata;
using NSubstitute;
using UniRx;

namespace Editor.GameplayTests.Mothers
{
    public static class GameRepositoryMother
    {
        public static GameRepository AGameRepository()
        {
            var repository = Substitute.For<GameRepository>();
            repository.GetWarriors().Returns(Observable.Return(Enumerable.Empty<Warrior>()));
            return repository;
        }
    }
}