using Gameplay.Domain.WarriorKata;

namespace Editor.GameplayTests.Mothers
{
    public static class WarriorMother
    {
        public static Warrior AWarrior(
            string withName = null,
            Weapons? withWeapons = null,
            WarriorClass? withClass = null
        ) {
            return new Warrior(
                withName ?? "",
                withWeapons ?? new Weapons(),
                withClass ?? WarriorClass.NotChosen
            );
        }
    }
}