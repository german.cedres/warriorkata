using Gameplay.Domain.WarriorKata;
using NSubstitute;
using NUnit.Framework;
using UniRx;
using static Editor.GameplayTests.Mothers.BuildWarriorMother;
using static Editor.GameplayTests.Mothers.WarriorBuilderServiceMother;
using static Editor.GameplayTests.Mothers.WarriorInProgressRepositoryMother;
using static Editor.GameplayTests.Mothers.WarriorMother;
using static Etermax.TestDriveRx.GivenContext;

namespace Editor.GameplayTests
{
    [TestFixture]
    public class BuildWarriorShould
    {
        
        [Test]
        public void EmitOnWarriorBuilt()
        {
            var onWarriorBuilt = new Subject<Warrior>();

             Given(ABuildWarrior(onWarriorBuilt))
                .When(action => action.Invoke())
                .Then(_ => onWarriorBuilt, it => it.EmittedOnce())
                .Run();
        }
        
        [Test]
        public void CallCreateEmptyWarriorInBuilder()
        {
            var warriorBuilderService = AWarriorBuilderService();
            
            Given(ABuildWarrior(withWarriorBuilderService: warriorBuilderService))
                .When(action => action.Invoke())
                .Then(_ => warriorBuilderService.Received(1).CreateEmptyWarrior())
                .Run();
        }
        
        //                          --- First part ---
        //behaviour tests 

        [Test]
        public void CallGetWarriorClassFromWarriorInProgressRepository()
        {
            var warriorInProgressRepository = AWarriorInProgressRepository();
            
            Given(ABuildWarrior(withWarriorInProgressRepository: warriorInProgressRepository))
                .When(action => action.Invoke())
                .Then(_ => warriorInProgressRepository.Received(1).GetClass())
                .Run();
        }
        
        [Test]
        public void CallAddWarriorClassInWarriorBuilderService()
        {
            var warriorBuilderService = AWarriorBuilderService();

            Given(ABuildWarrior(withWarriorBuilderService: warriorBuilderService))
                .When(action => action.Invoke())
                .Then(_ => warriorBuilderService.Received(1).AddWarriorClass(Arg.Any<Warrior>(), Arg.Any<WarriorClass>()))
                .Run();
        }
        
        //state tests
        
        [Test]
        public void BuildAWarriorWithWarriorClass()
        {
            var onWarriorBuilt = new Subject<Warrior>();
            
            //warrior fields
            var warriorClass = WarriorClass.Knight;
            
            //dependencies
            var warriorBuilderService = ALocalWarriorBuilderService();
            var warriorInProgressRepository = AInMemoryWarriorInProgressRepository(
                withWarriorClass: warriorClass
            );
            
            //expected
            var expectedWarrior = AWarrior(
                withClass: warriorClass
            );

            Given(ABuildWarrior(
                    withWarriorBuilderService: warriorBuilderService,
                    withWarriorInProgressRepository: warriorInProgressRepository,
                    withWarriorBuilt: onWarriorBuilt ))
                .When(action => action.Invoke())
                .Then(_ => onWarriorBuilt, it => it.EmittedExactly(expectedWarrior))
                .Run();
        }
        
        [Test]
        public void BuildAWarriorWithDifferentWarriorClass()
        {
            var onWarriorBuilt = new Subject<Warrior>();
            
            //warrior fields
            var warriorClass = WarriorClass.Mage;
            
            //dependencies
            var warriorBuilderService = ALocalWarriorBuilderService();
            var warriorInProgressRepository = AInMemoryWarriorInProgressRepository(
                withWarriorClass: warriorClass
            );
            
            //expected
            var expectedWarrior = AWarrior(
                withClass: warriorClass
            );

            Given(ABuildWarrior(
                    withWarriorBuilderService: warriorBuilderService,
                    withWarriorInProgressRepository: warriorInProgressRepository,
                    withWarriorBuilt: onWarriorBuilt ))
                .When(action => action.Invoke())
                .Then(_ => onWarriorBuilt, it => it.EmittedExactly(expectedWarrior))
                .Run();
        }
        
        //                          --- Second part ---
        //behaviour tests 

        [Test]
        public void CallGetNameFromWarriorInProgressRepository()
        {
            var warriorInProgressRepository = AWarriorInProgressRepository();
            
            Given(ABuildWarrior(withWarriorInProgressRepository: warriorInProgressRepository))
                .When(action => action.Invoke())
                .Then(_ => warriorInProgressRepository.Received(1).GetName())
                .Run();
        }
        
        [Test]
        public void CallAddNameInWarriorBuilderService()
        {
            var warriorBuilderService = AWarriorBuilderService();

            Given(ABuildWarrior(withWarriorBuilderService: warriorBuilderService))
                .When(action => action.Invoke())
                .Then(_ => warriorBuilderService.Received(1).AddName(Arg.Any<Warrior>(), Arg.Any<string>()))
                .Run();
        }
       
        // State tests
        
        [Test]
        public void CreateWarriorWithName()
        {
            var onWarriorBuilt = new Subject<Warrior>();
            
            //warrior fields
            var warriorName = "Warrior name";
            
            //dependencies
            var warriorBuilderService = ALocalWarriorBuilderService();
            var warriorInProgressRepository = AInMemoryWarriorInProgressRepository(
                withName: warriorName
            );
            
            //expected
            var expectedWarrior = AWarrior(
                withName: warriorName
            );

            Given(ABuildWarrior(
                    withWarriorBuilderService: warriorBuilderService,
                    withWarriorInProgressRepository: warriorInProgressRepository,
                    withWarriorBuilt: onWarriorBuilt ))
                .When(action => action.Invoke())
                .Then(_ => onWarriorBuilt, it => it.EmittedExactly(expectedWarrior))
                .Run();
        }
        [Test]
        public void CreateWarriorWithDifferentName()
        {
            var onWarriorBuilt = new Subject<Warrior>();
            
            //warrior fields
            var warriorName = "Elon Musk";
            
            //dependencies
            var warriorBuilderService = ALocalWarriorBuilderService();
            var warriorInProgressRepository = AInMemoryWarriorInProgressRepository(
                withName: warriorName
            );
            
            //expected
            var expectedWarrior = AWarrior(
                withName: warriorName
            );

            Given(ABuildWarrior(
                    withWarriorBuilderService: warriorBuilderService,
                    withWarriorInProgressRepository: warriorInProgressRepository,
                    withWarriorBuilt: onWarriorBuilt ))
                .When(action => action.Invoke())
                .Then(_ => onWarriorBuilt, it => it.EmittedExactly(expectedWarrior))
                .Run();
        }
        
        //                          --- Third part ---
        //behaviour tests 

        [Test]
        public void CallGetWeaponsFromWarriorInProgressRepository()
        {
            var warriorInProgressRepository = AWarriorInProgressRepository();

            Given(ABuildWarrior(withWarriorInProgressRepository: warriorInProgressRepository))
                .When(action => action.Invoke())
                .Then(_ => warriorInProgressRepository.Received(1).GetWeapons())
                .Run();
        }
        
        [Test]
        public void CallAddWeaponsFromWarriorBuilderService()
        {
            var warriorBuilderService = AWarriorBuilderService();

            Given(ABuildWarrior(withWarriorBuilderService: warriorBuilderService))
                .When(action => action.Invoke())
                .Then(_ => warriorBuilderService.Received(1).AddWeapons(Arg.Any<Warrior>(), Arg.Any<Weapons>()))
                .Run();
        }

        //State tests

        [Test]
        public void BuildAWarriorWithWeapons()
        {
            var onWarriorBuilt = new Subject<Warrior>();
            
            //warrior fields
            var warriorWeapons = new Weapons(" a lot");
            
            //dependencies
            var warriorBuilderService = ALocalWarriorBuilderService();
            var warriorInProgressRepository = AInMemoryWarriorInProgressRepository(
                withWeapons: warriorWeapons
            );
            
            //expected
            var expectedWarrior = AWarrior(
                withWeapons: warriorWeapons
            );

            Given(ABuildWarrior(
                    withWarriorBuilderService: warriorBuilderService,
                    withWarriorInProgressRepository: warriorInProgressRepository,
                    withWarriorBuilt: onWarriorBuilt ))
                .When(action => action.Invoke())
                .Then(_ => onWarriorBuilt, it => it.EmittedExactly(expectedWarrior))
                .Run();
        }
        
        [Test]
        public void BuildAWarriorWithDifferentWeapons()
        {
            var onWarriorBuilt = new Subject<Warrior>();
            
            //warrior fields
            var warriorWeapons = new Weapons("A lot of different weapons");
            
            //dependencies
            var warriorBuilderService = ALocalWarriorBuilderService();
            var warriorInProgressRepository = AInMemoryWarriorInProgressRepository(
                withWeapons: warriorWeapons
            );
            
            //expected
            var expectedWarrior = AWarrior(
                withWeapons: warriorWeapons
            );

            Given(ABuildWarrior(
                    withWarriorBuilderService: warriorBuilderService,
                    withWarriorInProgressRepository: warriorInProgressRepository,
                    withWarriorBuilt: onWarriorBuilt ))
                .When(action => action.Invoke())
                .Then(_ => onWarriorBuilt, it => it.EmittedExactly(expectedWarrior))
                .Run();
        }
        
        //              -----    Integration tests between all 3 parts      ------
        
        [Test]
        public void BuildAWarriorWithEverything()
        {
            var onWarriorBuilt = new Subject<Warrior>();
            
            //warrior fields
            var warriorName = "Warrior name";
            var warriorWeapons = new Weapons("a lot of weapons");
            var warriorClass = WarriorClass.Knight;
            
            //dependencies
            var warriorBuilderService = ALocalWarriorBuilderService();
            var warriorInProgressRepository = AInMemoryWarriorInProgressRepository(
                withName: warriorName,
                withWeapons: warriorWeapons,
                withWarriorClass: warriorClass
            );
            
            //expected
            var expectedWarrior = AWarrior(
                warriorName,
                warriorWeapons,
                warriorClass
            );

            Given(ABuildWarrior(
                    withWarriorBuilderService: warriorBuilderService,
                    withWarriorInProgressRepository: warriorInProgressRepository,
                    withWarriorBuilt: onWarriorBuilt ))
                .When(action => action.Invoke())
                .Then(_ => onWarriorBuilt, it => it.EmittedExactly(expectedWarrior))
                .Run();
        }
        
        [Test]
        public void BuildAWarriorWithEverythingDifferent()
        {
            var onWarriorBuilt = new Subject<Warrior>();
            
            //warrior fields
            var warriorName = "Elon Musk The Third";
            var warriorWeapons = new Weapons("Too many weapons to count");
            var warriorClass = WarriorClass.Mage;
            
            //dependencies
            var warriorBuilderService = ALocalWarriorBuilderService();
            var warriorInProgressRepository = AInMemoryWarriorInProgressRepository(
                withName: warriorName,
                withWeapons: warriorWeapons,
                withWarriorClass: warriorClass
            );
            
            //expected
            var expectedWarrior = AWarrior(
                warriorName,
                warriorWeapons,
                warriorClass
            );

            Given(ABuildWarrior(
                    withWarriorBuilderService: warriorBuilderService,
                    withWarriorInProgressRepository: warriorInProgressRepository,
                    withWarriorBuilt: onWarriorBuilt ))
                .When(action => action.Invoke())
                .Then(_ => onWarriorBuilt, it => it.EmittedExactly(expectedWarrior))
                .Run();
        }
    }
}