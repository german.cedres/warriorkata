using System;
using System.Collections.Generic;
using Gameplay.Domain.WarriorKata;

namespace Gameplay.Domain
{
    public interface GameRepository
    {
        IObservable<IEnumerable<Warrior>> GetWarriors();
        IEnumerable<float> GetHolyDamageMultiplier();
    }
}