using System.Collections.Generic;
using Gameplay.Domain.WarriorKata;
using UniRx;

namespace Gameplay.Domain.Actions

{
    public class CalculateWarriorsDps
    {
        readonly ISubject<IEnumerable<Warrior>> onWarriorsDpsCalculated;
        readonly GameRepository gameRepository;

        public CalculateWarriorsDps(
            ISubject<IEnumerable<Warrior>> onWarriorsDpsCalculated,
            GameRepository gameRepository
        ) {
            this.onWarriorsDpsCalculated = onWarriorsDpsCalculated;
            this.gameRepository = gameRepository;
        }

        public void Do()
        {
            gameRepository.GetWarriors()
                .Subscribe(onWarriorsDpsCalculated.OnNext);
        }
    }
}