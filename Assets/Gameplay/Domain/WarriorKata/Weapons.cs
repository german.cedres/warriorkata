namespace Gameplay.Domain.WarriorKata
{
    public readonly struct Weapons
    {
        readonly string description;

        public Weapons(string description)
        {
            this.description = description;
        }

        public override bool Equals(object obj)
        {
            if (obj is Weapons otherWeapons)
            {
                if (description == otherWeapons.description)
                {
                    return true;
                }

                return false;
            }

            return false;
        }

        public override string ToString()
        {
            return $"weapons description: {description}";
        }
    }
}