using System;
using UniRx;

namespace Gameplay.Domain.WarriorKata

{
    public class BuildWarrior
    {
        readonly IObserver<Warrior> onWarriorBuilt;
        readonly WarriorBuilderService warriorBuilderService;
        readonly WarriorInProgressRepository warriorInProgressRepository;

        public BuildWarrior(
            ISubject<Warrior> onWarriorBuilt, 
            WarriorBuilderService warriorBuilderService,
            WarriorInProgressRepository warriorInProgressRepository
        ) {
            this.onWarriorBuilt = onWarriorBuilt;
            this.warriorBuilderService = warriorBuilderService;
            this.warriorInProgressRepository = warriorInProgressRepository;
        }

        public void Invoke() =>
            warriorBuilderService.CreateEmptyWarrior()
                .Subscribe(onWarriorBuilt.OnNext);
    }
}