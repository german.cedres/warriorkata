using System;
using System.Collections.Generic;
using UniRx;

namespace Gameplay.Domain.WarriorKata
{
    public class ExternalGameRepository : GameRepository
    {
        IEnumerable<Warrior> units;

        public ExternalGameRepository(IEnumerable<Warrior> withWarriors)
        {
            units = withWarriors;
        }

        public IObservable<IEnumerable<Warrior>> GetWarriors()
        {
            return Observable.Return(units);
        }

        public IEnumerable<float> GetHolyDamageMultiplier()
        {
            throw new NotImplementedException();
        }
    }
}