using System;

namespace Gameplay.Domain.WarriorKata
{
    public interface WarriorBuilderService
    {
        IObservable<Warrior> CreateEmptyWarrior();
        IObservable<Warrior> AddName(Warrior warrior, string name);
        IObservable<Warrior> AddWarriorClass(Warrior warrior, WarriorClass warriorClass);
        IObservable<Warrior> AddWeapons(Warrior warrior, Weapons weapons);
    }
}