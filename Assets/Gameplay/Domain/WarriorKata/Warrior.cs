using System;

namespace Gameplay.Domain.WarriorKata
{
    [Serializable]
    public struct Warrior
    {
        public readonly string warriorName;
        public readonly Weapons weapons;
        public readonly WarriorClass warriorClass;
        
        //unused
        public  readonly int holyDamage;
        public   readonly int undeadDamage ;
        public readonly int physicalDamage ;
        public readonly WarriorType warriorType;

        public Warrior(
            string warriorName,
            Weapons weapons,
            WarriorClass warriorClass
        ) {
            this.warriorName = warriorName;
            this.weapons = weapons;
            this.warriorClass = warriorClass;

            // unused
            physicalDamage = 0;
            undeadDamage = 0;
            holyDamage = 0;
            warriorType = WarriorType.Holy;
        }

        public override bool Equals(object obj)
        {
            if (obj is Warrior otherWarrior)
            {
                if (warriorName == otherWarrior.warriorName &&
                    weapons.Equals(otherWarrior.weapons) &&
                    warriorClass == otherWarrior.warriorClass
                ) {
                    return true;
                }
            }
            return false;
        }

        public override string ToString() =>
            $"Warrior name: {warriorName} \n" +
            $"Warrior weapons: {weapons} \n" +
            $"Warrior class: {warriorClass} \n";
    }
}