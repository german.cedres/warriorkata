namespace Gameplay.Domain.WarriorKata
{
    public enum WarriorClass
    {
        Knight, Mage, Sage, Berserk, NotChosen
    }
}