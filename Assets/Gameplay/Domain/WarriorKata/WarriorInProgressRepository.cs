using System;

namespace Gameplay.Domain.WarriorKata
{
    public interface WarriorInProgressRepository
    {
        IObservable<WarriorClass> GetClass();
        IObservable<string> GetName();
        IObservable<Weapons> GetWeapons();
    }
}