using System;
using UniRx;

namespace Gameplay.Domain.WarriorKata
{
    public class InMemoryWarriorInProgressRepository : WarriorInProgressRepository
    {
        WarriorClass warriorClass;
        string warriorName;
        Weapons weapons;

        public InMemoryWarriorInProgressRepository(
            WarriorClass withWarriorClass,
            string withName,
            Weapons withWeapons
        )
        {
            warriorClass = withWarriorClass;
            warriorName = withName ;
            weapons = withWeapons;
        }

        public IObservable<WarriorClass> GetClass() => 
            Observable.Return(warriorClass);

        public IObservable<string> GetName() => 
            Observable.Return(warriorName);

        public IObservable<Weapons> GetWeapons() => 
            Observable.Return(weapons);
    }
}