using System;
using System.Collections.Generic;
using Gameplay.Domain.WarriorKata;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace Gameplay.Views.Views
{
    public class GameView : MonoBehaviour
    {
        [SerializeField] TextMeshProUGUI DPSAmount;
        [SerializeField] TextMeshProUGUI DamageDoneAmount;

        [SerializeField] WarriorView warriorPrefab;
        [SerializeField] Transform warriorsContent;
    
        [SerializeField] Button BlessButton;
        [SerializeField] Button CurseButton;
        [SerializeField] Button BerserkButton;

        int totalDps = 0;

        public void UpdateDPS(int dps)
        {
            DPSAmount.text = dps.ToString();
            totalDps += dps;
            DamageDoneAmount.text = totalDps.ToString();
        }

        public IObservable<Unit> OnBlessButtonPressed => BlessButton.OnClickAsObservable();
        public IObservable<Unit> OnCurseButtonPressed => CurseButton.OnClickAsObservable();
        public IObservable<Unit> OnBerserkButtonPressed => BerserkButton.OnClickAsObservable();

        public void CreateWarriors(IEnumerable<Warrior> warriors)
        {
            foreach (Transform oldWarrior in warriorsContent) Destroy(oldWarrior.gameObject);

            foreach (var warrior in warriors)
            {
                var newWarrior = Instantiate(warriorPrefab, warriorsContent);
                newWarrior.Initialize(warrior);
            }
        }
    }
}
