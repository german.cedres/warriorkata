using UnityEngine;

namespace Gameplay.Views.Views
{
    public class GameApplicationView : MonoBehaviour
    {
        // Start is called before the first frame update
        [SerializeField] TimelineView timelineView;
        [SerializeField] GameView gameView;
    

        void Start()
        {
            InitializeGameplay();
        }

        void InitializeGameplay()
        {
            var context = new Context();
            context.Initialize(timelineView, gameView);
        }
    }
}