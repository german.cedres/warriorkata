using System;
using System.Collections.Generic;
using UniRx;
using Views;

public class Context
{
    public void Initialize(TimelineView timelineView, GameView gameView)
    {
        var presenter = new Presenter(timelineView, gameView);
        DoSubscriptions(presenter);
    }

    void DoSubscriptions(Presenter presenter)
    {
        PrepareForDisposition(new CompositeDisposable(), presenter.Disposables);
    }
    static void PrepareForDisposition(CompositeDisposable disposables, IEnumerable<IDisposable> subscriptions)
    {
        foreach (var subscription in subscriptions) subscription.AddTo(disposables);
    }
}