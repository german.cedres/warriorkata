using System;
using System.Collections.Generic;
using Actions;
using UniRx;
using Views;
using WarriorKata;

public class Presenter
{
    readonly TimelineView timelineView;
    readonly GameView gameView;
    
    readonly DoDamage doDamage;
    readonly CalculateWarriorsDps calculateWarriorsDps;
    
    readonly ISubject<int> onDamageDone;
    readonly ISubject<IEnumerable<Warrior>> onWarriorsDpsCalculated;

    public Presenter(TimelineView timelineView, GameView gameView)
    {
        this.timelineView = timelineView;
        this.gameView = gameView;
        var gameRepository= new ExternalGameRepository(
            withWarriors: new[]
            {
                new Warrior(),                
                new Warrior(),                
            }
        );
        
        // events
        onDamageDone = new Subject<int>();
        onWarriorsDpsCalculated = new Subject<IEnumerable<Warrior>>();
        
        // actions
        doDamage = new DoDamage(onDamageDone);
        calculateWarriorsDps = new CalculateWarriorsDps(onWarriorsDpsCalculated, gameRepository);
    }

    public IEnumerable<IDisposable> Disposables => 
      new[]
      {
          ASecondInGameHasPassed,
          OnWarriorsDpsCalculated,
          OnDamageDone,
          
          OnCurseButtonPressed,
          OnBlessButtonPressed,
          OnBerserkButtonPressed
      };

    IDisposable ASecondInGameHasPassed =>
        timelineView.OnOneSecondPassed
            .Subscribe(_ => calculateWarriorsDps.Do());

    IDisposable OnWarriorsDpsCalculated =>
        onWarriorsDpsCalculated
            .Do(warriors => gameView.CreateWarriors(warriors))
            .Subscribe(warriors => doDamage.Do(warriors));
    IDisposable OnDamageDone =>
        onDamageDone
            .Subscribe(gameView.UpdateDPS);

    IDisposable OnCurseButtonPressed =>
        gameView.OnCurseButtonPressed
            .Subscribe();    
    
    IDisposable OnBlessButtonPressed =>
        gameView.OnBlessButtonPressed
            .Subscribe();   
    
    IDisposable OnBerserkButtonPressed =>
        gameView.OnCurseButtonPressed
            .Subscribe();

}