using System;
using UniRx;
using UnityEngine;

namespace Views
{
    public class TimelineView : MonoBehaviour
    {
        public readonly ISubject<float> OnOneSecondPassed = new Subject<float>();
        int timeLineInterval = 1;

        void Start() => 
            StartTimeline();

        void StartTimeline() =>
            Observable.Interval(TimeSpan.FromSeconds(timeLineInterval))
                .Subscribe(_ => OnOneSecondPassed.OnNext(timeLineInterval));
    }
}
