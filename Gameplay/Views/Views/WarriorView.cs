using TMPro;
using UnityEngine;
using WarriorKata;

namespace Views
{
    public class WarriorView : MonoBehaviour
    {
        [SerializeField] int HolyDamage;
        [SerializeField] int UndeadDamage;
        [SerializeField] int PhysicalDamage;
        [SerializeField] WarriorType warriorType;

        [SerializeField] TextMeshProUGUI dpsText;
        [SerializeField] TextMeshProUGUI nameText;
  
        public void Initialize(Warrior warrior)
        {
            HolyDamage = warrior.holyDamage;
            UndeadDamage = warrior.undeadDamage;
            PhysicalDamage = warrior.physicalDamage;
            warriorType = warrior.warriorType;
            
            nameText.text = warriorType.ToString() + " Warrior";
            dpsText.text = (HolyDamage + UndeadDamage + PhysicalDamage).ToString();
        }
    }

    public enum WarriorType
    {
        Holy, Undead
    }
}