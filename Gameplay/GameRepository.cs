using System;
using System.Collections.Generic;
using Views;
using WarriorKata;

public interface GameRepository
{
    IObservable<IEnumerable<Warrior>> GetWarriors();
    IEnumerable<float> GetHolyDamageMultiplier();
}