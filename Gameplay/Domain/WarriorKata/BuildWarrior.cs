using System;
using UniRx;

namespace WarriorKata

{
    public class BuildWarrior
    {
        readonly ISubject<Warrior> onWarriorBuilt;
        readonly WarriorBuilderService warriorBuilderService;
        readonly WarriorInProgressRepository withWarriorInProgressRepository;

        public BuildWarrior(
            ISubject<Warrior> onWarriorBuilt, 
            WarriorBuilderService warriorBuilderService,
            WarriorInProgressRepository withWarriorInProgressRepository
        ) {
            this.onWarriorBuilt = onWarriorBuilt;
            this.warriorBuilderService = warriorBuilderService;
            this.withWarriorInProgressRepository = withWarriorInProgressRepository;
        }

        public void Invoke() =>
            warriorBuilderService.CreateEmptyWarrior()
                .Subscribe(onWarriorBuilt);
    }
}