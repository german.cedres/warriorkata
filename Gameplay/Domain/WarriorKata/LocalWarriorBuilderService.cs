using System;
using UniRx;

namespace WarriorKata
{
    public class LocalWarriorBuilderService : WarriorBuilderService
    {
        public IObservable<Warrior> CreateEmptyWarrior()
        {
            return Observable.Return(new Warrior());
        }

        public IObservable<Warrior> AddName(Warrior warrior, string name) =>
            Observable.Return(new Warrior(
                warriorName: name,
                weapons: warrior.weapons,
                warriorClass: warrior.warriorClass)
            );

        public IObservable<Warrior> AddWarriorClass(Warrior warrior, WarriorClass warriorClass)=>
            Observable.Return(new Warrior(
                warriorName: warrior.warriorName,
                weapons: warrior.weapons,
                warriorClass: warriorClass)
            );

        public IObservable<Warrior> AddWeapons(Warrior warrior, Weapons weapons)=>
            Observable.Return(new Warrior(
                warriorName: warrior.warriorName,
                weapons: weapons,
                warriorClass: warrior.warriorClass)
            );
    }
}