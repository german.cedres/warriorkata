using System;

namespace WarriorKata
{
    public interface WarriorInProgressRepository
    {
        IObservable<WarriorClass> GetClass();
        IObservable<string> GetName();
        IObservable<Weapons> GetWeapons();
    }
}