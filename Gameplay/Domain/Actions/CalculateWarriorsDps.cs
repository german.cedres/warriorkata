using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using Views;
using WarriorKata;

namespace Actions

{
    public class CalculateWarriorsDps
    {
        readonly ISubject<IEnumerable<Warrior>> onWarriorsDpsCalculated;
        readonly GameRepository gameRepository;

        public CalculateWarriorsDps(
            ISubject<IEnumerable<Warrior>> onWarriorsDpsCalculated,
            GameRepository gameRepository
        ) {
            this.onWarriorsDpsCalculated = onWarriorsDpsCalculated;
            this.gameRepository = gameRepository;
        }

        public void Do()
        {
            gameRepository.GetWarriors()
                .Subscribe(onWarriorsDpsCalculated.OnNext);
        }
    }
}