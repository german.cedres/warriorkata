using System.Collections.Generic;
using UniRx;
using Views;
using WarriorKata;

namespace Actions
{
    public class DoDamage
    {
        readonly ISubject<int> onDamageDone;

        public DoDamage(ISubject<int> onDamageDone)
        {
            this.onDamageDone = onDamageDone;
        }

        public void Do(IEnumerable<Warrior> warriors)
        {
            var damage = 0;
            foreach (var warrior in warriors)
            {
                damage += warrior.holyDamage;
                damage += warrior.undeadDamage;
                damage += warrior.physicalDamage;
            }
            onDamageDone.OnNext(damage);
        }
    }
}