# WarriorKata



## Ejercicio de construir un guerrero

El ejercicio consiste en completar una accion que crea un guerrero en el juego en base a lo que el jugador previamente habia elegido en una "fabrica" in game.


## Contexto

- -> Te mandan a arreglar un bug. El bug proviene del armado de la accion donde se fetchea y se crea el guerrero. Hay racing conditions, tests en rojo. Decidis Borrar todo
- -> El set de tests esta correcto, entonces lo unico que falta es comenzar de cero a escribir para que todo pase
- -> Una de las complejidades proviene de que todos los metodos necesarios para acceder a la data son Observables (decision tecnica legacy)

## Limitantes

- [ ] No se pueden cambiar las firmas de los repos o los servicios
- [ ] Se tiene que resolver todo dentro de BuildWarrior.cs en el metodo Invoke()
- [ ] No se pueden modificar/borrar tests


## Hints

- [ ] Tener en cuenta el metodo SelectMany
- [ ] Tener en cuenta la query From In https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/keywords/from-clause
- [ ] El from in se puede utilizar como Select Many pero para no tener que pasar tuplas, utilizando varios from se puede generar ¨variables¨locales. Estas selecciones (lo from, de lo in por asi decir) son la data dentro del observable, pero luego cuando se hace el select se vuelve a convertir en observable (async). Ej; from cars in repository.GetCars(): la variable cars va a ser por ejemplo un IEnumerable, mientras que getCars es un observable.
//